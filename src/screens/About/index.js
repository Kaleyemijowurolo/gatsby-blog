import React from "react"
import { Link } from "gatsby"
import SEO from "../../components/Seo/seo"
import Layout from "../../components/Layout/layout"

const About = () => {
  return (
    <Layout>
      <SEO title="About" />
      <title>Contact Us</title>
      <p>This is my about page</p>
      <Link to={"/"}>HOme</Link>
    </Layout>
  )
}
export default About
