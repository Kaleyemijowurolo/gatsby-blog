import React from "react"
import { Link } from "gatsby"
import SEO from "../../components/Seo/seo"
import Layout from "../../components/Layout/layout"

const Contact = () => {
  return (
    <Layout>
      <SEO title="Contact" />
      <p>This is my contact page</p>
      <Link to={"/"}>HOme</Link>
    </Layout>
  )
}
export default Contact
