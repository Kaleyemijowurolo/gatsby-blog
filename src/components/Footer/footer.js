import React from "react"
import {Footer} from './footer'

const _Footer = () => {
  return (
    <Footer
      style={{
        marginTop: `2rem`,
      }}
    >
      © {new Date().getFullYear()}, Built with
      {` `}
      <a href="https://www.gatsbyjs.com">
        Gatsby,
        <a href="https://www.gatsbyjs.com"> by: Kayode</a>
      </a>
    </Footer>
  )
}
export default _Footer
